package Archive::BagIt::Base;
use strict;
use warnings;
use Moo;
extends "Archive::BagIt";
# VERSION
# ABSTRACT: deprecated, used for backwards compatibility

=head1 NAME

Archive::BagIt::Base - only for backwards compatibility needed

=cut

__PACKAGE__->meta->make_immutable;

1;
