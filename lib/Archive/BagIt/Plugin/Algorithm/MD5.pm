package Archive::BagIt::Plugin::Algorithm::MD5;
use strict;
use warnings;
use Carp qw( croak );
use Moo;
use namespace::autoclean;
with 'Archive::BagIt::Role::Algorithm', 'Archive::BagIt::Role::OpenSSL';
# VERSION
# ABSTRACT: The MD5 algorithm plugin (default for v0.97)

has '+plugin_name' => (
    is => 'ro',
    default => 'Archive::BagIt::Plugin::Algorithm::MD5',
);

has '+name' => (
    is      => 'ro',
    #isa     => 'Str',
    default => 'md5',
);



__PACKAGE__->meta->make_immutable;
1;
