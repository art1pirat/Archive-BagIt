package Archive::BagIt::Plugin::Algorithm::SHA512;
use strict;
use warnings;
use Carp qw( croak );
use Moo;
use namespace::autoclean;
with 'Archive::BagIt::Role::Algorithm';
with 'Archive::BagIt::Role::OpenSSL';
# VERSION
# ABSTRACT: The default SHA algorithms plugin (default for v1.0)

has '+plugin_name' => (
    is => 'ro',
    default => 'Archive::BagIt::Plugin::Algorithm::SHA512',
);

has '+name' => (
    is      => 'ro',
    #isa     => 'Str',
    default => 'sha512',
);

__PACKAGE__->meta->make_immutable;
1;
