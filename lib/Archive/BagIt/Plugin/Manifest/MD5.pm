package Archive::BagIt::Plugin::Manifest::MD5;
# ABSTRACT: The role to load md5 plugin (default for v0.97)
# VERSION

use strict;
use warnings;
use Moo;
with 'Archive::BagIt::Role::Manifest';

has '+plugin_name' => (
    is => 'ro',
    default => 'Archive::BagIt::Plugin::Manifest::MD5',
);

has 'manifest_path' => (
    is => 'ro',
);

has 'manifest_files' => (
    is => 'ro',
);

has '+algorithm' => (
    is => 'rw',
);

sub BUILD {
    my ($self) = @_;
    $self->bagit->load_plugins(("Archive::BagIt::Plugin::Algorithm::MD5"));
    $self->algorithm($self->bagit->plugins->{"Archive::BagIt::Plugin::Algorithm::MD5"});
    return 1;
}

1;
