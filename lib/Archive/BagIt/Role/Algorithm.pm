package Archive::BagIt::Role::Algorithm;
use strict;
use warnings;
use Moo::Role;
use Carp qw(croak);
with 'Archive::BagIt::Role::Plugin';
# ABSTRACT: A role that defines the interface to a hashing algorithm
# VERSION

has 'name' => (
    is => 'ro',
);

=pod

=head2 get_optimal_bufsize($fh)

uses L<stat> to determine optimal filesize, defaults to 8192

=cut

sub get_optimal_bufsize {
    my ($self, $fh) = @_;
    my ($dev,$ino,$mode,$nlink,$uid,$gid,$rdev,$size,
        $atime,$mtime,$ctime,$blksize,$blocks)
        = stat $fh;
    # Windows systems return "" for $blksize
    if ((defined $blksize ) && ($blksize ne "") && ($blksize >= 512)) {
        return $blksize;
    }
    return 8192;
}

sub register_plugin {
    my ($class, $bagit) =@_;
    my $self = $class->new({bagit=>$bagit});
    my $plugin_name = $self->plugin_name;
    $self->bagit->plugins( { $plugin_name => $self });
    $self->bagit->algos( {$self->name => $self });
    return 1;
}
no Moo;
1;
