package Archive::BagIt::Role::OpenSSL;
use strict;
use warnings;
use Archive::BagIt::Role::OpenSSL::Sync;
use Class::Load qw(load_class);
use Carp qw(carp);
use Moo::Role;
use namespace::autoclean;
# ABSTRACT: A role that handles plugin loading
# VERSION

=pod

=head2 has_async_support()

returns true if async IO is possible, because IO::Async could be loaded, otherwise returns false

=cut

has 'async_support' => (
    is        => 'ro',
    builder   => '_check_async_support',
    predicate => 1,
    lazy      => 1,
);

sub _check_async_support {
    my $self = shift;
    if (! exists $INC{'IO/Async.pm'}) {
        carp "Module 'IO::Async' not available, disable async support";
        $self->bagit->use_async(0);
        return 0;
    }
    load_class('IO::Async');
    return 1;
}



sub _get_hash_string_sync {
    my ($self, $fh, $blksize)=@_;
    my $obj = Archive::BagIt::Role::OpenSSL::Sync->new( name => $self->name);
    return $obj->calc_digest($fh, $blksize);
}

sub _get_hash_string_async {
    my ($self, $fh, $blksize) = @_;
    my $result;
    if ($self->has_async_support()) {
        my $class = 'Archive::BagIt::Role::OpenSSL::Async';
        load_class($class) or croak("could not load class $class");
        my $obj = $class->new(name => $self->name);
        $result = $obj->calc_digest($fh, $blksize);
    } else {
        $result = $self->_get_hash_string_sync($fh, $blksize);
    }
    return $result;
}

=pod

=head2 get_hash_string($fh)

calls synchronous or asynchronous function to calc digest of file, depending on result of $bag->use_async()
returns the digest result as hex string

=cut

sub get_hash_string {
    my ($self, $fh) = @_;
    my $blksize = $self->get_optimal_bufsize($fh);
    my $bagobj = $self->bagit;
    if ($bagobj->use_async) {
        return $self->_get_hash_string_async($fh, $blksize);
    }
    return $self->_get_hash_string_sync($fh, $blksize);
}


no Moo;
1;