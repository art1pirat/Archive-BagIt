package Archive::BagIt::Role::Plugin;
use strict;
use warnings;
use Moo::Role;
use namespace::autoclean;
# ABSTRACT: A role that handles plugin loading
# VERSION

has plugin_name => (
  is  => 'ro',
  #isa => 'Str',
  default => __PACKAGE__,
);

=head2 bagit()

holds the current bag object as weak reference

=cut

has bagit => (
  is  => 'ro',
  #isa => 'Archive::BagIt',
  required => 1,
  weak_ref => 1,
);

sub BUILD {
    my ($self) = @_;
    my $plugin_name = $self->plugin_name;
    $self->bagit->plugins( { $plugin_name => $self });
    return 1;
}
no Moo;
1;
